import { Component, OnInit } from '@angular/core';
import { Form, NgForm } from '@angular/forms';
import * as firebase from 'firebase';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  onSubmit(form: NgForm) {
    const fullname = form.value.fullname;
    const email = form.value.email;
    const password = form.value.password;

    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(userData => {
      firebase.auth().currentUser.sendEmailVerification();
      return firebase.database().ref('users/' + firebase.auth().currentUser.uid).set({
        email: email,
        uid: firebase.auth().currentUser.uid,
        registrationDate: new Date().toString(),
        name: fullname
      }).then(() => {
        firebase.auth().signOut();
      });
    })
    .catch(err => {
      console.log(err);
    });

  }

}
