import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  title = 'instagram-like';

  ngOnInit(): void {
    const firebaseConfig = {
      apiKey: 'AIzaSyBAW4ye6ikh0qZWuhPoRkFSHyPMP1qjy0o',
      authDomain: 'instagram-d34b9.firebaseapp.com',
      databaseURL: 'https://instagram-d34b9.firebaseio.com',
      projectId: 'instagram-d34b9',
      storageBucket: 'instagram-d34b9.appspot.com',
      messagingSenderId: '51866095740',
      appId: '1:51866095740:web:c9a5781b28b03172'
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }

}
